#include "SubstrateSD.hh"

#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"



SubstrateSD::SubstrateSD(const G4String& name,
                         const G4String& hitsCollectionName)
: G4VSensitiveDetector(name),
  fHitsCollection(NULL)
{
    collectionName.insert(hitsCollectionName);
}



SubstrateSD::~SubstrateSD()
{}



void SubstrateSD::Initialize(G4HCofThisEvent* hce)
{
    // Create hits collection
    fHitsCollection =
        new SubstrateHitsCollection(SensitiveDetectorName, collectionName[0]);

    // Add this collection in hce
    G4int hcID =
        G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    hce->AddHitsCollection( hcID, fHitsCollection );
}



G4bool SubstrateSD::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
    // energy deposit
    G4double edep = aStep->GetTotalEnergyDeposit();

    if (edep==0.) return false;

    SubstrateHit* newHit = new SubstrateHit();

    newHit->SetTrackID (aStep->GetTrack()->GetTrackID());
    newHit->SetEdep    (edep);
    newHit->SetPos     (aStep->GetPostStepPoint()->GetPosition());

    fHitsCollection->insert( newHit );

    //newHit->Print();

    return true;
}



void SubstrateSD::EndOfEvent(G4HCofThisEvent*)
{
    if ( verboseLevel>1 ) {
        G4int nofHits = fHitsCollection->entries();
        G4cout << G4endl
               << "--------> Hits Collection: "
               << nofHits
               << " hits in the substrate"
               << G4endl;
     for ( G4int i=0; i<nofHits; i++ )
        (*fHitsCollection)[i]->Print();
    }
}
