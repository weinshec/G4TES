#include "EventAction.hh"
#include "Analysis.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4SDManager.hh"



EventAction::EventAction()
: G4UserEventAction(),
  fSubstrateHCID(-1)
{}



EventAction::~EventAction()
{}



SubstrateHitsCollection*
EventAction::GetHitsCollection(G4int hcID,
                               const G4Event* event) const
{
    SubstrateHitsCollection* hitsCollection =
        static_cast<SubstrateHitsCollection*>(
            event->GetHCofThisEvent()->GetHC(hcID));

    if (!hitsCollection) {
        G4ExceptionDescription msg;
        msg << "Cannot access hitsCollection ID " << hcID;
        G4Exception("EventAction::GetHitsCollection()",
                    "fooCode",
                    FatalException, msg);
    }

    return hitsCollection;
}



void EventAction::BeginOfEventAction(const G4Event*)
{
}



void EventAction::EndOfEventAction(const G4Event* event)
{
    // Get HitsCollectionID only once
    if (fSubstrateHCID == -1) {
        fSubstrateHCID = G4SDManager::GetSDMpointer()
            ->GetCollectionID("SubstrateHitsCollection");
    }

    // Get the actual HitsCollection
    SubstrateHitsCollection* substrHC =
        GetHitsCollection(fSubstrateHCID, event);

    // Return when no Hit took place
    if (substrHC->entries() < 1) return;

    // Get the hit Instance
    G4double Edep_total = 0.0;
    for(int i = 0; i < substrHC->entries(); i++) {
        SubstrateHit* hit = (*substrHC)[i];
        Edep_total += hit->GetEdep();
    }

    SubstrateHit* hit = (*substrHC)[0];

    // Fill histograms and ntuples
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->FillNtupleDColumn(0, Edep_total);
    analysisManager->FillNtupleDColumn(1, hit->GetPos().x());
    analysisManager->FillNtupleDColumn(2, hit->GetPos().y());
    analysisManager->AddNtupleRow();
}
