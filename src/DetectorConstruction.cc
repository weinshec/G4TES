#include "DetectorConstruction.hh"
#include "SubstrateSD.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"


DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fWorldMaterial(NULL),
  fSubstrMaterial(NULL),
  fCheckOverlaps(true)
{ }



DetectorConstruction::~DetectorConstruction()
{ }



G4VPhysicalVolume* DetectorConstruction::Construct()
{
    DefineMaterials();
    return DefineVolumes();
}



void DetectorConstruction::DefineMaterials()
{
    G4NistManager* nist = G4NistManager::Instance();

    fWorldMaterial  = nist->FindOrBuildMaterial("G4_AIR");
    fSubstrMaterial = nist->FindOrBuildMaterial("G4_Si");

    G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}



G4VPhysicalVolume* DetectorConstruction::DefineVolumes()
{
    //
    // Define Dimensions
    //
    G4double worldSize    = 10*mm;
    G4double substrRadius = 2.497*mm/2.;
    G4double substrWidth  = 0.275*mm;

    //
    // World
    //
    G4Box* worldSolid = new G4Box("worldSolid",
                                  0.5*worldSize,
                                  0.5*worldSize,
                                  0.5*worldSize);

    G4LogicalVolume* worldLogic = new G4LogicalVolume(worldSolid,
                                                      fWorldMaterial,
                                                      "worldLogic");

    G4VPhysicalVolume* worldPhys =
        new G4PVPlacement(0,                     //no rotation
                          G4ThreeVector(),       //at (0,0,0)
                          worldLogic,            //its logical volume
                          "World",               //its name
                          0,                     //its mother  volume
                          false,                 //no boolean operation
                          0,                     //copy number
                          fCheckOverlaps);       //overlaps checking

    //
    // Substrate
    //
    G4Tubs* substrSolid = new G4Tubs("substrSolid",
                                     0.0,          // R_min
                                     substrRadius, // R_max
                                     substrWidth,  // height
                                     0.*deg,       // start angle
                                     360.*deg);    // end angle

    G4LogicalVolume* substrLogic =
        new G4LogicalVolume(substrSolid,        //its solid
                            fSubstrMaterial,    //its material
                            "substrLogic");       //its name

    new G4PVPlacement(0,                       //no rotation
                      G4ThreeVector(),         //at position
                      substrLogic,             //its logical volume
                      "Substrate",             //its name
                      worldLogic,              //its mother  volume
                      false,                   //no boolean operation
                      0,                       //copy number
                      fCheckOverlaps);          //overlaps checking

    G4VisAttributes* substrVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0));
    substrLogic->SetVisAttributes(substrVisAtt);

  return worldPhys;
}



void DetectorConstruction::ConstructSDandField()
{
    //
    // Sensitive detectors
    //
    G4String substrSDname = "muon_simple/substrSD";
    SubstrateSD* substrSD = new SubstrateSD(substrSDname,
                                            "SubstrateHitsCollection");
    SetSensitiveDetector("substrLogic", substrSD, false);
}
