#include "SubstrateHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include <iomanip>

G4ThreadLocal G4Allocator<SubstrateHit>* SubstrateHitAllocator=0;



SubstrateHit::SubstrateHit()
 : G4VHit(),
   fTrackID(-1),
   fEdep(0.),
   fPos(G4ThreeVector())
{}



SubstrateHit::~SubstrateHit() {}



SubstrateHit::SubstrateHit(const SubstrateHit& right)
  : G4VHit()
{
    fTrackID   = right.fTrackID;
    fEdep      = right.fEdep;
    fPos       = right.fPos;
}



const SubstrateHit& SubstrateHit::operator=(const SubstrateHit& right)
{
    fTrackID   = right.fTrackID;
    fEdep      = right.fEdep;
    fPos       = right.fPos;

    return *this;
}



G4int SubstrateHit::operator==(const SubstrateHit& right) const
{
    return ( this == &right ) ? 1 : 0;
}



void SubstrateHit::Draw()
{
    G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
    if(pVVisManager)
    {
        G4Circle circle(fPos);
        circle.SetScreenSize(4.);
        circle.SetFillStyle(G4Circle::filled);
        G4Colour colour(1.,0.,0.);
        G4VisAttributes attribs(colour);
        circle.SetVisAttributes(attribs);
        pVVisManager->Draw(circle);
    }
}



void SubstrateHit::Print()
{
    G4cout << " trackID: " << fTrackID
           << " Edep: "
           << std::setw(7) << G4BestUnit(fEdep,"Energy")
           << " Position: "
           << std::setw(7) << G4BestUnit(fPos,"Length")
           << G4endl;
}
