#include "RunAction.hh"
#include "Analysis.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"



RunAction::RunAction()
: G4UserRunAction()
{
    // set printing progress number per each 1000 events
    G4RunManager::GetRunManager()->SetPrintProgress(1000);

    // Create analysis manager
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    G4cout << "Using " << analysisManager->GetType() << G4endl;

    // Create directories
    //analysisManager->SetHistoDirectoryName("histograms");
    //analysisManager->SetNtupleDirectoryName("ntuple");
    analysisManager->SetVerboseLevel(1);
    // analysisManager->SetFirstHistoId(1);

    // Book histograms, ntuples, ...
    analysisManager->CreateNtuple("tracks", "tracks");
    analysisManager->CreateNtupleDColumn("Edep");
    analysisManager->CreateNtupleDColumn("posX");
    analysisManager->CreateNtupleDColumn("posY");
    analysisManager->FinishNtuple();
}



RunAction::~RunAction()
{
    delete G4AnalysisManager::Instance();
}



void RunAction::BeginOfRunAction(const G4Run*)
{
    //inform the runManager to save random number seed
    //G4RunManager::GetRunManager()->SetRandomNumberStore(false);

    // Get analysis manager
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    // Open an output file
    G4String filename = "muon_simple";
    analysisManager->OpenFile(filename);
}



void RunAction::EndOfRunAction(const G4Run*)
{
    // save histograms & ntuple
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->Write();
    analysisManager->CloseFile();
}
