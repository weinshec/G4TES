#ifndef SUBSTRATESD_H
#define SUBSTRATESD_H

#include "G4VSensitiveDetector.hh"

#include "SubstrateHit.hh"

#include <vector>

class G4Step;
class G4HCofThisEvent;

/// The hits are accounted in hits in ProcessHits() function which is called
/// by Geant4 kernel at each step. A hit is created with each step with non zero
/// energy deposit.

class SubstrateSD : public G4VSensitiveDetector
{
  public:
    SubstrateSD(const G4String& name,
                const G4String& hitsCollectionName);
    virtual ~SubstrateSD();

    // methods from base class
    virtual void   Initialize(G4HCofThisEvent* hitCollection);
    virtual G4bool ProcessHits(G4Step* step, G4TouchableHistory* history);
    virtual void   EndOfEvent(G4HCofThisEvent* hitCollection);

  private:
    SubstrateHitsCollection* fHitsCollection;
};

#endif /* SUBSTRATESD_H */
