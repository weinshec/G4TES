#ifndef DETECTORCONSTRUCTION_H
#define DETECTORCONSTRUCTION_H

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

class G4VPhysicalVolume;
class G4Material;



class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    DetectorConstruction();
    virtual ~DetectorConstruction();

    virtual G4VPhysicalVolume* Construct();
    virtual void ConstructSDandField();


  private:
    void DefineMaterials();
    G4VPhysicalVolume* DefineVolumes();

    // materials
    G4Material* fWorldMaterial;
    G4Material* fSubstrMaterial;

    G4bool  fCheckOverlaps; // option to activate checking of volumes overlaps
};


#endif
