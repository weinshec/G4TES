#ifndef SUBSTRATEHIT_H
#define SUBSTRATEHIT_H

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "tls.hh"

/// It defines data members to store the trackID, energy deposit,
/// and position of charged particles in a selected volume:

class SubstrateHit : public G4VHit
{
  public:
    SubstrateHit();
    SubstrateHit(const SubstrateHit&);
    virtual ~SubstrateHit();

    // operators
    const SubstrateHit& operator=(const SubstrateHit&);
    G4int operator==(const SubstrateHit&) const;

    inline void* operator new(size_t);
    inline void  operator delete(void*);

    // methods from base class
    virtual void Draw();
    virtual void Print();

    // Set methods
    void SetTrackID  (G4int track)      { fTrackID = track; };
    void SetEdep     (G4double de)      { fEdep = de; };
    void SetPos      (G4ThreeVector xyz){ fPos = xyz; };

    // Get methods
    G4int         GetTrackID() const { return fTrackID; };
    G4double      GetEdep()    const { return fEdep; };
    G4ThreeVector GetPos()     const { return fPos; };

  private:
      G4int         fTrackID;
      G4double      fEdep;
      G4ThreeVector fPos;
};


typedef G4THitsCollection<SubstrateHit> SubstrateHitsCollection;


extern G4ThreadLocal G4Allocator<SubstrateHit>* SubstrateHitAllocator;


inline void* SubstrateHit::operator new(size_t)
{
    if(!SubstrateHitAllocator)
        SubstrateHitAllocator = new G4Allocator<SubstrateHit>;
    return (void *) SubstrateHitAllocator->MallocSingle();
}


inline void SubstrateHit::operator delete(void *hit)
{
    SubstrateHitAllocator->FreeSingle((SubstrateHit*) hit);
}

#endif /* SUBSTRATEHIT_H */
