#ifndef EVENTACTION_H
#define EVENTACTION_H

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "SubstrateHit.hh"

/// Event action class
///

class EventAction : public G4UserEventAction
{
  public:
    EventAction();
    virtual ~EventAction();

    virtual void BeginOfEventAction(const G4Event* event);
    virtual void EndOfEventAction(const G4Event* event);

  private:
    SubstrateHitsCollection* GetHitsCollection(G4int hcID,
                                               const G4Event* event) const;
    G4int fSubstrateHCID;
};


#endif /* EVENTACTION_H */
